@echo off
:: Install Apache HTTP Server 2.2.25
:: Start Variables

set httpd_version=2.2.25
set url=http://corentsoftwares.s3.amazonaws.com/windows/Apache/For_64bit/httpd-2.2.25-ssl-x64.zip
set file_name=httpd-2.2.25-ssl-x64.zip
set zip_cmd=7z x
set project_dir=C:\
set httpd_dir=%project_dir%Apache22

if exist "%httpd_dir%" (
	echo+
	echo an existing Apache22 installation found on %httpd_dir%
	echo Please remove the folder %httpd_dir% and restart the installation script
	echo+
	goto quit
	)
:: End Variables

:: Actual Script Starts Here!
echo+
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if not exist "%file_name%" (
	echo+
	echo Downloading %url%
	powershell -Command "(New-Object Net.WebClient).DownloadFile('%url%', '%file_name%')"
	)

if not exist "%httpd_dir%" (
	echo+
	echo Extracting Apache22 %httpd_dir%
	%zip_cmd% %file_name% -o%project_dir%
	if exist "%project_dir%readme_first.html" (
		del %project_dir%readme_first.html /Q
		)
	)

echo %PATH% | find /i "%httpd_dir%\bin" > NUL && echo %httpd_dir%\bin found in PATH || SETX /M Path "%httpd_dir%\bin;%PATH%"

start /wait %httpd_dir%\bin\httpd.exe -k install && start /wait %httpd_dir%\bin\httpd.exe -k start
start http://localhost/

:quit
echo+
pause
exit
