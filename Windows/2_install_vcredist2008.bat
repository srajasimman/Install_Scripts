@echo off
:: Install Microsoft Visual C++ 2008 SP1 Redistributable
:: Start Variables

set version=vcredist_x86_2008
set url_x86=https://download.microsoft.com/download/5/D/8/5D8C65CB-C849-4025-8E95-C3966CAFD8AE/vcredist_x86.exe
set file_name_x86=vcredist_x86.exe
if not exist "%file_name_x86%" (
	echo+
	echo Downloading %url_x86%
	powershell -Command "(New-Object Net.WebClient).DownloadFile('%url_x86%', '%file_name_x86%')"
	)

start /wait %file_name_x86% /Q

set version=vcredist_x64_2008
set url_x64=https://download.microsoft.com/download/5/D/8/5D8C65CB-C849-4025-8E95-C3966CAFD8AE/vcredist_x64.exe
set file_name_x64=vcredist_x64.exe
if not exist "%file_name_x64%" (
	echo+
	echo Downloading %url_x64%
	powershell -Command "(New-Object Net.WebClient).DownloadFile('%url_x64%', '%file_name_x64%')"
	)

start /wait %file_name_x64% /Q
echo Microsoft Visual C++ 2008 SP1 Redistributable Installation Done
pause