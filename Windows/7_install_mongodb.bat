@echo off
:: Install MongoDB - install_mongodb.bat ver_#
:: Ex: install_mongodb.bat 2.2.0
:: Start Variables

if "%1"=="" (
	echo+
	echo 	Enter mongodb version to install
	echo 	Ex: install_mongodb 2.2.0 or install_mongodb 3.2.3
	echo+
	goto quit
	)

set mongodb_version=%1
set url=http://downloads.mongodb.org/win32/mongodb-win32-x86_64-%mongodb_version%.zip
set file_name=mongodb-win32-x86_64-%mongodb_version%
set zip_cmd=7z x
set project_dir=C:\All_Projects
set mongodb_dir=%project_dir%\mongodb

if exist "%mongodb_dir%" (
	echo+
	echo an existing mongodb installation found on %mongodb_dir%
	echo Please remove the folder %mongodb_dir% and restart the installation script
	echo+
	goto quit
	)
set mongodb_data_dir=C:\data\db

:: End Variables

:: Actual Script Starts Here!
echo+
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if not exist "%file_name%.zip" (
	echo+
	echo Downloading %url%
	powershell -Command "(New-Object Net.WebClient).DownloadFile('%url%', '%file_name%.zip')"
	)

if not exist "%mongodb_dir%\bin" (
	echo+
	echo Extracting MongoDB %mongodb_dir%
	%zip_cmd% %file_name%.zip -o%project_dir%
	move %project_dir%\mongodb-win32-x86_64-%mongodb_version% %project_dir%\mongodb
	)

if not exist "%mongodb_dir%\log" (
	echo+
	echo CREATING FOLDER %mongodb_dir%
	mkdir "%mongodb_dir%\log"
	)

if not exist "%mongodb_data_dir%" (
	echo+
	echo CREATING FOLDER %mongodb_data_dir%
	mkdir "%mongodb_data_dir%"
	)

if not exist "%mongodb_dir%\mongod.cfg" (
	echo+
	echo Adding Options in Config file %mongodb_dir%\mongod.cfg
	echo logpath=%mongodb_dir%\log\mongo.log > %mongodb_dir%\mongod.cfg
	echo dbpath=%mongodb_data_dir%\ >> %mongodb_dir%\mongod.cfg
	)

echo %PATH% | find /i "%mongodb_dir%" > NUL && echo %mongodb_dir% found in PATH || SETX /M Path "%PATH%;%mongodb_dir%\bin"
	
start /wait %mongodb_dir%\bin\mongod.exe --auth --config %mongodb_dir%\mongod.cfg --install
net start MongoDB

:quit
echo+
pause
exit
