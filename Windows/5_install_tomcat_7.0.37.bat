@echo off
:: Install Apache Tomcat 7.0.37
:: Start Variables

set tomcat_version=7.0.37
set url=https://archive.apache.org/dist/tomcat/tomcat-7/v7.0.37/bin/apache-tomcat-7.0.37-windows-x64.zip
set file_name=apache-tomcat-7.0.37-windows-x64.zip
7z > NUL && set zip_cmd=7z x || echo 7-Zip Command not found in System PATH
set project_dir=C:\All_Projects\
set tomcat_dir=%project_dir%apache-tomcat-7.0.37\

if exist "%tomcat_dir%" (
	echo+
	echo an existing Tomcat installation found on %tomcat_dir%
	echo Please remove the folder %tomcat_dir% and restart the installation script
	echo+
	goto quit
	)
:: End Variables

:: Actual Script Starts Here!
echo+
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if not exist "%file_name%" (
	echo+
	echo Downloading %url%
	powershell -Command "(New-Object Net.WebClient).DownloadFile('%url%', '%file_name%')"
	)

if not exist "%project_dir%" (
	echo+
	mkdir %project_dir%
	)
if not exist %tomcat_dir% (
	echo Extracting Tomcat %tomcat_dir%
	%zip_cmd% %file_name% -o%project_dir%
	)
start %tomcat_dir%\bin\

:quit
echo+
pause
