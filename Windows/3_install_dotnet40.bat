@echo off
:: Install Dot Net 4.0 Framework Full Redistributable
:: Start Variables

set dotnet_version=1.8
set file_name=dotNetFx40_Full_x86_x64.exe
set url=https://download.microsoft.com/download/9/5/A/95A9616B-7A37-4AF6-BC36-D6EA96C8DAAE/%file_name%

if not exist "%file_name%" (
	echo+
	echo Downloading %url%
	powershell -Command "(New-Object Net.WebClient).DownloadFile('%url%', '%file_name%')"
	)

start /wait %file_name% /q /norestart /log %temp%\DN40.html
start %temp%\DN40.html

echo Dot Net 4.0 Framework Full Redistributable Installation Done
pause