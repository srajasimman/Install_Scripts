@echo off
:: Install MySQL (Community Server) 5.7.11
:: Start Variables

set file_name=mysql-installer-community-5.7.11.0.msi
set url=https://downloads.mysql.com/archives/get/file/%file_name%

set installer="C:\Program Files (x86)\MySQL\MySQL Installer for Windows\MySQLInstallerConsole.exe"

set setup_type=server
set version=5.7.11
set arc=X64
set mysql_port=3306
set root_passwd=MySQL@123
echo Installing MySQL (Community Server) 5.7.11

if not exist "%file_name%" (
	echo+
	echo Downloading %url%
	powershell -Command "(New-Object Net.WebClient).DownloadFile('%url%', '%file_name%')"
	)
echo+

if not exist %installer% (
	echo Installing MySQL Installer Service...
	start /wait msiexec /i %file_name% /qn /norestart
	)

%installer% community install server;%version%;%arc%:*:port=%mysql_port%;openfirewall=true;rootpasswd=%root_passwd% -silent

echo MySQL (Community Server) 5.7.11 Installation Done
echo+
echo -----------------------
echo Details
echo -----------------------
echo Server User: root
echo+
echo Server Password: %root_passwd%
echo+
echo Version: %version%
echo+
echo MySQL Port: %mysql_port%

:quit
pause