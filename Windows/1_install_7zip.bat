@echo off
:: Install 7-Zip - install_7zip.bat
:: Ex: install_7zip.bat
:: Start Variables

:: Find Windows Oprating System Type 64bit or 32bit OS
reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT
:: End Variables
@echo off
7z > NUL && GOTO quit

:: Actual Script Starts Here!
if %OS%==32BIT (
	echo This is a 32bit operating system
	set file_name=7z1604.exe
	)

if %OS%==64BIT (
	echo This is a 64bit operating system
	set file_name=7z1604-x64.exe
	)
set url=http://www.7-zip.org/a/%file_name%

if not exist "%file_name%" (
	echo+
	echo Downloading %url%
	powershell -Command "(New-Object Net.WebClient).DownloadFile('%url%', '%file_name%')"
	)

start /wait %file_name% /S

echo %PATH% | find /i "7-zip" > NUL && echo 7-Zip found in PATH || SETX /M Path "%ProgramFiles%\7-Zip;%PATH%"
7z > NUL && echo 7-Zip Command added into System PATH || echo 7-Zip Command not added into System PATH

:quit
pause
exit
