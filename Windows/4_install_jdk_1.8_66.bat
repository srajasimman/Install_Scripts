@echo off
:: Install JDK 1.8_66
:: Start Variables

set java_version=1.8
set java_install_path=C:\Program Files\Java\jdk1.8.0_66
set file_name=jdk-8u66-windows-x64.exe
set url=http://corentsoftwares.s3.amazonaws.com/windows/Java/%java_version%/%file_name%

if not exist "%file_name%" (
	echo+
	echo Downloading %url%
	powershell -Command "(New-Object Net.WebClient).DownloadFile('%url%', '%file_name%')"
	)

start /wait %file_name% /s

echo %PATH% | find /i "%java_install_path%\bin" > NUL && echo JAVA_PATH found in PATH || SETX /M Path "%java_install_path%\bin;%PATH%"
SETX /M JAVA_PATH "%java_install_path%\bin"
SETX /M JAVA_HOME "%java_install_path%"
echo Java Installed on %java_install_path%
pause